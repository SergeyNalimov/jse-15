package ru.nalimov.tm.controller;

import ru.nalimov.tm.entity.User;
import ru.nalimov.tm.enumerated.Role;
import ru.nalimov.tm.repository.UserRepository;
import ru.nalimov.tm.service.UserService;
import ru.nalimov.tm.util.HashMD5;

public class UserController extends AbstractController {

 //   private static UserController instance;

    private final UserRepository userRepository = new UserRepository();

    private UserService userService = new UserService(userRepository);

    private final SystemController systemController = new SystemController();

    private User appUser;

    public UserController(UserService userService) {
        this.userService = userService;
    }


  /*  public static UserController getInstance() {
        synchronized (UserController.class) {
            return instance == null ? instance = new UserController() : instance;
        }
    }*/

    public User getAppUser() {
        return appUser;
    }

    public void setAppUser(User appUser) {
        this.appUser = appUser;
    }

    public int createUser() {
        System.out.println("[CREATE STANDART USER]");
        System.out.println("PLEASE, ENTER LOGIN");
        final String login = scanner.nextLine();
        System.out.println("PLEASE, ENTER PASSWORD");
        final String password = scanner.nextLine();
        System.out.println("PLEASE, ENTER FIRST NAME");
        final String firstName = scanner.nextLine();
        System.out.println("PLEASE, ENTER SECOND NAME");
        final String secondName = scanner.nextLine();
        System.out.println("PLEASE, ENTER LAST NAME");
        final String lastName = scanner.nextLine();
        userService.create(login, password, firstName, secondName, lastName);
        System.out.println("[OK]");
        return 0;
    }

    public int createUser(Role userRole) {
        System.out.println("[CREATE ADMIN]");
        System.out.println("PLEASE, ENTER LOGIN");
        final String login = scanner.nextLine();
        System.out.println("PLEASE, ENTER PASSWORD");
        final String password = scanner.nextLine();
        System.out.println("PLEASE, ENTER FIRST NAME");
        final String firstName = scanner.nextLine();
        System.out.println("PLEASE, ENTER SECOND NAME");
        final String secondName = scanner.nextLine();
        System.out.println("PLEASE, ENTER LAST NAME");
        final String lastName = scanner.nextLine();
        userService.create(login, password, firstName, secondName, lastName, userRole);
        System.out.println("[OK]");
        return 0;
    }

    public int updateUserByLogin(){
        System.out.println("[UPDATE USER]");
        System.out.println("PLEASE, ENTER LOGIN");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER PASSWORD");
        final String password = scanner.nextLine();
        System.out.println("PLEASE, ENTER FIRST NAME");
        final String firstName = scanner.nextLine();
        System.out.println("PLEASE, ENTER SECOND NAME");
        final String secondName = scanner.nextLine();
        System.out.println("PLEASE, ENTER LASt NAME");
        final String lastName = scanner.nextLine();
        userService.updateByLogin(login, password, firstName, secondName, lastName);
        System.out.println("[OK]");
        return 0;
    }

    public int clearUsers() {
        System.out.println("[CLEAR USERS]");
        userService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public int removeUserByLogin() {
        System.out.println("[REMOVE USER BY LOGIN]");
        System.out.println("PLEASE, ENTER LOGIN");
        final String login = scanner.nextLine();
        final User user = userService.removeByLogin(login);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int listUsers() {
        System.out.println("[LIST USERS]");
        int index = 1;
        for (final User user: userService.findAll()) {
            System.out.println(index + ". " + user.getId() + " "  + user.getLogin() + ": " + user.getHashPassword() + " " + user.getUserRole());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    public void viewUser(final User user) {
        if (user == null) return;
        System.out.println("[VIEW USER DETAIL]");
        System.out.println("USER ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("PASSWORD HASH: " + user.getHashPassword());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("SECOND NAME: " + user.getSecondName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("ROLE: " + user.getUserRole());
        System.out.println("[OK]");
    }

    public int viewUserByLogin() {
        System.out.println("ENTER LOGIN:");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        this.viewUser(user);
        return 0;
    }

    public int viewUserById() {
        System.out.println("PLEASE, ENTER USER ID:");
        final long id = scanner.nextLong();
        final User user = userService.findById(id);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        this.viewUser(user);
        return 0;
    }

    public int updateUserById(){
        System.out.println("[UPDATE USER]");
        System.out.println("PLEASE, ENTER USER ID:");
        final long id = scanner.nextLong();
        final User user = userService.findById(id);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER LOGIN:");
        final String login = scanner.nextLine();
        System.out.println("PLEASE, ENTER USER ID:");
        final String password = scanner.nextLine();
        System.out.println("PLEASE, ENTER FIRST NAME");
        final String firstName = scanner.nextLine();
        System.out.println("PLEASE, ENTER SECOND NAME");
        final String secondName = scanner.nextLine();
        System.out.println("PLEASE, ENTER LAST NAME");
        final String lastName = scanner.nextLine();
        userService.updateById(id, login, password, firstName, secondName, lastName);
        System.out.println("[OK]");
        return 0;
    }

    public int removeUserById() {
        System.out.println("[REMOVE USER BY ID]");
        System.out.println("PLEASE, ENTER USER ID:");
        final long id = scanner.nextLong();
        final User user = userService.removeById(id);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int userAuthentic(){
        System.out.println("[AUTHENTIC]");
        System.out.println("PLEASE, ENTER LOGIN:");
        final String login = scanner.nextLine();
        System.out.println("PLAESE, ENTER PASSWORD:");
        final String password = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        if (userService.userAuthentic(login, password) == null){
            System.out.println("[FAIL]");
            return 0;
        }

        return 0;
    }

    public int userDeauthentic(){
        System.out.println("[DEAUTHENTIC]");
        userService.userDeauthentic();
        System.out.println("[OK]");
        return 0;
    }

    public int userChangePassword(){
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("PLEASE, ENTER LOGIN:");
        final String login = scanner.nextLine();
        System.out.println("PLEASE, ENTER OLD PASSWORD");
        final String oldPassword = scanner.nextLine();
        System.out.println("PLEASE, ENTER NEW PASSWORD");
        final String newPassword = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        if (!user.getHashPassword().equals(HashMD5.getHash(oldPassword))) {
            System.out.println("[FAIL]");
            return 0;
        }
        user.setHashPassword(newPassword);
        System.out.println("[OK]");
        return 0;
    }

    public int userProfile(final Long id) {
        if (id == null) return -1;
        System.out.println("CURRENT SESSION:");
        System.out.println("ID:" + id.toString());
        System.out.println("LOGIN: " + userService.findById(id).getLogin());
        System.out.println("FIRSTNAME:" + userService.findById(id).getFirstName());
        System.out.println("SECONDNAME:" + userService.findById(id).getSecondName());
        System.out.println("LASTNAME:" + userService.findById(id).getLastName());
        System.out.println("ROLE:" + userService.findById(id).getUserRole());
        return 0;
    }

    public String isLoginExists() {
        System.out.println("PLEASE, ENTER YOUR NEW LOGIN:");
        final String login = scanner.nextLine();
        if (login == null || login.isEmpty()) {
            System.out.println("LOGIN MAST NOT BE EMPTY!");
            System.out.println("FAIL");
            return null;
        };
        if (userService.findByLogin(login) != null) {
            System.out.println("THIS LOGIN EXISTS!");
            System.out.println("FAIL");
            return null;
        };
        return login;
    }

    public int updateProfile(final Long id) {
        final String login = isLoginExists();
        if (login == null) {
            return -1;
        }
        else {
            System.out.println("PLEASE, ENTER YOUR NEW FIRSTNAME:");
            final String firstname = scanner.nextLine();
            System.out.println("PLEASE, ENTER YOUR NEW SECONDNAME:");
            final String secondname = scanner.nextLine();
            System.out.println("PLEASE, ENTER YOUR NEW LASTNAME:");
            final String lastname = scanner.nextLine();
            userService.updateProfile(id, login, firstname, secondname, lastname);
            System.out.println("PROFILE UPDATED");
            userProfile(id);
            return 0;
        }
    }

    public int updateRole(Long id) {
        System.out.println("[UPDATE USER ROLE]");
        if (userService.findById(id).isAdmin_true() == true) {
            System.out.println("PLEASE, ENTER UPDATE USER LOGIN:");
            final String login = scanner.nextLine();
            final User user = userService.findByLogin(login);
            if (user == null) {
                System.out.println("FAIL");
                return 0;
            }
            System.out.println("PLEASE, ENTER ROLE: ADMIN OR USER");
            final String role = scanner.nextLine();
            if (role.equals("ADMIN") || role.equals("USER")) {
                userService.updateRole(login, role);
                System.out.println("OK");
                return 0;
            } else {
                System.out.println("UNKNOWN ROLE!");
                return 0;
            }

        }
        else {
            systemController.displayForAdminOnly();
            return -1;
        }
    }

}

